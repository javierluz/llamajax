;function Llamajax(url, printContainer, urlContainer) {
    if(url) this.url = url; else this.url = "#";
    if(printContainer) this.printContainer = printContainer; else this.printContainer = "#llamajax";
    if(urlContainer) this.urlContainer = urlContainer; else this.urlContainer = false;

    this.llamar = function() {
        var _this = this;
        var urlGet = _this.url
        if(_this.urlContainer) urlGet += _this.urlContainer;

        /* Hacemos el llamado AJAX con la data que definamos*/
        $.ajax({
            url: 'dist/grabber.php',
            data: {url: _this.url},
            cache: false,
            method: "POST",
            context: document.body,
            dataType: "html"
        }).done(function( html ) {
            console.log( "Applicanddo éxito" );
            $( _this.printContainer ).empty();
            $( _this.printContainer ).append( html );
        })
        .fail(function() {
            console.log( "Applicanddo error" );
        })
        .always(function(data) {
            console.log( data );
        });

    }
};