<?php
function file_get_contents_utf8($fn) {
    $content = file_get_contents($fn);
     
    return mb_convert_encoding($content, 'UTF-8',
        mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));
}

$getUrl = $_POST['url'];

if(!$getUrl) {
    die('<p style="text-align:center; color:#393939; font-family:\'Ubuntu\', Helvetica, Arial, sans-serif; margin-top:5em;">ERROR: No se pueden enviar datos directamente.</p>');
}

$url = htmlspecialchars($getUrl, ENT_QUOTES);
$url = preg_replace( "/^\.+|\.+$/", "", $url );

echo file_get_contents_utf8($url);

?>